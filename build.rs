extern crate cc;
extern crate pkg_config;

fn main() {
    let mingw = std::env::var("TARGET").unwrap_or(String::new()).contains("mingw");

    cc::Build::new()
        .file("src/encoder.c")
        .compile("encoder");

    pkg_config::Config::new().statik(mingw).probe("libavformat libavcodec libswscale libavutil").unwrap();
}
