#!/bin/bash

echo 'Please type the twitch channel name you wish to retrieve:'
read twitchchannel
echo "Generating files for channel: $twitchchannel"

#REMOVE OLD FILES
rm res/badges.json
rm res/bttv_emotes.json
rm res/bttv_channel.json
rm res/channel_ffz_emotes.json
rm res/global_ffz_emotes.json

#GENERATE NEW FILES
wget -O res/badges.json "https://api.twitch.tv/kraken/chat/$twitchchannel/badges" --header='Client-Id: kimne78kx3ncx6brgo4mv6wki5h1ko'
wget -O res/bttv_emotes.json "https://api.betterttv.net/2/emotes"
wget -O res/bttv_channel.json "https://api.betterttv.net/2/channels/$twitchchannel"
wget -O res/channel_ffz_emotes.json "https://api.frankerfacez.com/v1/room/$twitchchannel"
wget -O res/global_ffz_emotes.json "https://api.frankerfacez.com/v1/set/global"

clear

echo 'Files have been generated and placed in the res folder.'
echo 'You may now close this window now. :)'

read
